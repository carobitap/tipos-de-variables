// Nancy Carolina García Medina
// ES1611300623
// 29 de Julio 2021
// Unidad 1 Actividad 2 

using System;
 
 namespace TiposDeVariables
 {
    public class operadores
    {
        public static void Main()
        {
            //Declaración de variables enteras
            int firstNum, secondNum;
    
            //Lectura del primer número
            Console.Write("Introduce un número: ");
            firstNum = Convert.ToInt32(Console.ReadLine());
            
            // Lectura del segundo número
            Console.Write("Introduce otro número: ");
            secondNum = Convert.ToInt32(Console.ReadLine());
    
            //Condicional con operadores lógicos y relacionales
            if ((firstNum > 0) && (secondNum > 0))
                Console.WriteLine("Ambos números son positivos.");
            else
                Console.WriteLine("Al menos uno no es positivo.");
        }
    }
 }